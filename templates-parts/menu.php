<div id="mainMenu">

  <nav class="navbar navbar-default">
    <div class="container">
      
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/">
                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/logotipo-guaguamedia.png" alt="logo GuaguaMedia" /> <span class="text-logo"><?php bloginfo( 'name' ); ?></span>
              </a>
        </div>

        <?php 

          wp_nav_menu( array(
            'container'       => false, 
            'items_wrap'      => '<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1"><ul id="%1$s" class="nav navbar-nav navbar-right">%3$s</ul></div>'
            ) );

        ?>

      </div>
  </nav>

</div>