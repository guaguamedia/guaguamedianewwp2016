    <div id="search">
        <div class="row">
            <div class="col-lg-12">
                <h2>Buscador</h2>
                <form role="search" method="get" class="form-search" action="<?php echo home_url() ; ?>/">
                    <input type="text" class="search-field" placeholder="" value="<?php echo esc_html($s, 1); ?>" name="s" id="s">
                    <button class="search-submit" type="submit" value="">
                        <i class="fa fa-search"></i>
                    </button>
                </form>
            </div>
        </div>
    </div>