<div class="grid">
  <div class="grid-sizer"></div>
<?php
$argsPortafolioHome = array(
  'post_type' => 'portafolio',
  'nopaging'  => 'true',
  'meta_key'  => 'orden',
  'orderby'   => 'meta_value_num',
  'order'     => 'DESC'
);

// query
$queryPortafolioHome = new WP_Query( $argsPortafolioHome );

if($queryPortafolioHome->have_posts()):

  while ( $queryPortafolioHome->have_posts() ) : $queryPortafolioHome->the_post();
  ?>
    <?php if(get_field('destacado')): ?>
      <div class="grid-item">
        <a href="<?php the_permalink() ?>">
            <?php the_post_thumbnail(array(390, 700), array( 'class'=>' img-responsive')); ?>
        </a>
        <?php if(false): ?>
        <div class="caption">
          <div class="contentCaption">
              <h2 class="hidden"><?php the_title(); ?></h2>
              <?php 
              $logotipo = get_field('logotipo');
              if( !empty($logotipo) ): ?>
                <img src="<?php echo $logotipo['url']; ?>" alt="<?php echo $logotipo['alt']; ?>" class="img-responsive center-block" />
              <?php else: ?>
              <h2><?php the_title(); ?></h2>
              <?php endif; ?>
              
              <div>
                <ul class="list-inline">
                  <?php if(get_field('url')): ?>
                  <li>
                      <a href="<?php echo get_field('url') ?>" target="_blank"><i class="wow pulse fa fa-link"></i></a>
                  </li>
                  <?php endif; ?>
                  <li>
                      <a href="<?php the_permalink() ?>"><i class="wow pulse fa fa-eye"></i></a>
                  </li>
                </ul>
              </div>
          </div>
        </div>
        <?php endif; ?>
      </div>
<?php
    endif;
  endwhile;
endif;
wp_reset_query();
?>