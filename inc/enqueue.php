<?php 
/**
 * Enqueue scripts and styles.
 */
function guaguamedia_scripts() {
    global $version;

    //style
    wp_enqueue_style( 'style', get_stylesheet_uri(), array(), $version);

    wp_deregister_script( 'jquery' );
    wp_register_script( 'jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js', "", '1.12.4', false);

    //wow
    wp_enqueue_script( 'wow-js', get_template_directory_uri() . '/components/wow/dist/wow.js', array('jquery'), $version, false);
    wp_enqueue_style( 'wow-css', get_template_directory_uri() . '/components/wow/css/libs/animate.css', array(), $version);

    //owl
    wp_enqueue_style( 'owl-carousel-css', get_template_directory_uri() . '/components/owl/owl-carousel/owl.carousel.css', array(), $version);
    wp_enqueue_style( 'owl-theme-css', get_template_directory_uri() . '/components/owl/owl-carousel/owl.theme.css', array(), $version);
    wp_enqueue_script( 'owl-carousel-js', get_template_directory_uri() . '/components/owl/owl-carousel/owl.carousel.min.js', array('jquery'), $version, false);

    //lightcase
    wp_enqueue_style( 'lightcase-css', get_template_directory_uri() . '/components/lightcase/src/css/lightcase.css', array(), $version);
    wp_enqueue_script( 'lightcase-js', get_template_directory_uri() . '/components/lightcase/src/js/lightcase.js', array('jquery'), $version, true);

    //bootstrap
    wp_enqueue_style( 'bootstrap-css', get_template_directory_uri() . '/components/bootstrap/dist/css/bootstrap.min.css', array(), $version);
    wp_enqueue_style( 'bootstrap-theme-css', get_template_directory_uri() . '/components/bootstrap/dist/css/bootstrap-theme.min.css', array(), $version);
    wp_enqueue_script( 'bootstrap-js', get_template_directory_uri() . '/components/bootstrap/dist/js/bootstrap.min.js', array('jquery'), $version, true);

    //masonry
    if (is_home()):
        wp_enqueue_script( 'masonry-js', get_template_directory_uri() . '/components/masonry/dist/masonry.pkgd.min.js', array('jquery'), $version, true);
    endif;

    //font awesome
    wp_enqueue_style( 'font-awesome-css', get_template_directory_uri() . '/components/font-awesome/css/font-awesome.min.css', array(), $version);

    //imagesloaded
    if (is_home()):
        wp_enqueue_script( 'imagesloaded-js', get_template_directory_uri() . '/components/imagesloaded/imagesloaded.pkgd.min.js', array('jquery'), $version, true);
    endif;

    //script
    wp_enqueue_script( 'script-js', get_template_directory_uri() . '/js/script-min.js', array('jquery'), $version, true );

    //styles
    wp_enqueue_style( 'styles-css', get_stylesheet_directory_uri() . '/css/main-min.css', array(), $version);


    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment-reply' );
    }
}
add_action( 'wp_enqueue_scripts', 'guaguamedia_scripts' );