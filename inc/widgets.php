<?php 

function guaguamedia_widgets_init() {
	// require get_template_directory() . '/inc/widgets.php';
	// register_widget( 'Twenty_Fourteen_Ephemera_Widget' );

	register_sidebar( array(
		'name'          => __( 'Left', 'guaguamedia' ),
		'id'            => 'left',
		'description'   => __( 'Izquierdo', 'guaguamedia' ),
		'before_widget' => '<div>',
		'after_widget'  => '</div>'
	) );

	register_sidebar( array(
		'name'          => __( 'Center', 'guaguamedia' ),
		'id'            => 'center',
		'description'   => __( 'Centro', 'guaguamedia' ),
		'before_widget' => '<div>',
		'after_widget'  => '</div>'
	) );

	register_sidebar( array(
		'name'          => __( 'Right', 'guaguamedia' ),
		'id'            => 'right',
		'description'   => __( 'Derecho', 'guaguamedia' ),
		'before_widget' => '<div>',
		'after_widget'  => '</div>'
	) );
	
	register_sidebar( array(
		'name'          => __( 'Copy', 'guaguamedia' ),
		'id'            => 'copy',
		'description'   => __( 'Copy', 'guaguamedia' ),
		'before_widget' => '<div>',
		'after_widget'  => '</div>'
	) );

}
add_action( 'widgets_init', 'guaguamedia_widgets_init' );

?>