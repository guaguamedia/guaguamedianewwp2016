<?php

global $version;
$version = '2.0.2';

//excerpt
function new_excerpt_more( $more ) {
    return ' <div class="read-more"><a href="' . get_permalink( get_the_ID() ) . '">' . __( 'Mas Informaci&oacute;n <span class="glyphicon glyphicon-circle-arrow-right"></span>', 'guaguamedia' ) . '</a></div>';
}
add_filter( 'excerpt_more', 'new_excerpt_more' );

//eliminar error scripts
add_action( 'wp_default_scripts', function( $scripts ) {
    if ( ! empty( $scripts->registered['jquery'] ) ) {
        $jquery_dependencies = $scripts->registered['jquery']->deps;
        $scripts->registered['jquery']->deps = array_diff( $jquery_dependencies, array( 'jquery-migrate' ) );
    }
} );

//Boton buscador
add_filter('wp_nav_menu_items','search_box_function', 10, 2);
function search_box_function( $nav, $args ) {
    return $nav.'<li><a href="#search" data-rel="lightcase"><i class="fa fa-search"></i></a></li>';
}

//Slug
function the_slug($echo=true){
  $slug = basename(get_permalink());
  do_action('before_slug', $slug);
  $slug = apply_filters('slug_filter', $slug);
  if( $echo ) echo $slug;
  do_action('after_slug', $slug);
  return $slug;
}

/* Defer */
function add_defer_attribute($tag, $handle) {
    // add script handles to the array below
    $scripts_to_defer = array(
        'masonry-pkgd-js',
        'imagesloaded-js',
        'owl-carousel-js',
        'bootstrap-js',
        'plugins-min.js',
        'jquery-fancybox-js',
        'wow-js',
        'script-js'
    );

    foreach($scripts_to_defer as $defer_script) {
        if ($defer_script === $handle) {
            return str_replace(' src', ' defer="defer" src', $tag);
        }
    }
    return $tag;
}
add_filter('script_loader_tag', 'add_defer_attribute', 10, 2);

/* Async */
function add_async_attribute($tag, $handle)
{
    // add script handles to the array below
    $scripts_to_async = array(
        'jquery-1.12.4.js',
    );

    foreach ($scripts_to_async as $async_script) {
        if ($async_script === $handle) {
            return str_replace(' src', ' async="async" src', $tag);
        }
    }
    return $tag;
}

add_filter('script_loader_tag', 'add_async_attribute', 10, 2);

?>