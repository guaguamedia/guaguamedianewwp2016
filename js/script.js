var $ = jQuery.noConflict();
$( document ).ready(function() {
	"use restrict";
	
	// init Masonry
	var $grid = $('.grid').masonry({
	  itemSelector: '.grid-item',
	  percentPosition: true,
	  columnWidth: '.grid-sizer'
	});
	// layout Isotope after each image loads
	$grid.imagesLoaded().progress( function() {
	  $grid.masonry();
	}); 

	// lightcase
	$('a[data-rel^=lightcase]').lightcase({
		showSequenceInfo: false,
		inline: {
			width: 500,
			height: 200
		},
		swipe: true,
    onFinish : {
      baz: function() {
        $("#s").focus();
      }
    }
	});

	$('a[data-rel^=portafolio]').lightcase({	
		showTitle: false,
		transition: 'elastic',
		maxHeight: 'auto'
	});

	// owl
	$("#owl-demo").owlCarousel({
 
      navigation : false, // Show next and prev buttons
      slideSpeed : 300,
      paginationSpeed : 400,
      singleItem:true
 
      // "singleItem:true" is a shortcut for:
      // items : 1, 
      // itemsDesktop : false,
      // itemsDesktopSmall : false,
      // itemsTablet: false,
      // itemsMobile : false
 
  	});

  	// wow
  	var wow = new WOW({
        animateClass: 'animated',
        offset:       100
    });
    wow.init();

});