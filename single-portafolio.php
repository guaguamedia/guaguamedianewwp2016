<?php get_header(); ?>
    <!-- menu -->
    <?php get_template_part("templates-parts/menu"); ?>
    <!-- fin menu -->
  
    <!-- header -->
    <header>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1><?php the_title(); ?></h1>
                    <?php if(get_field('url')): ?>
                    <h2><?php the_field('url'); ?></h2>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </header>
    <!-- fin header -->

    <!-- main -->
    <section id="main">
        <div class="container">

            <div class="row content-portafolio">
                <div class="col-sm-6">

                    <?php if( have_rows('imagenes') ): ?>
                        <div id="owl-demo" class="owl-carousel owl-theme">
                        <?php
                            while ( have_rows('imagenes') ) : the_row();
                                $imagen = get_sub_field('imagen');
                                $size = array(600, 1000); // (thumbnail, medium, large, ` or custom size)
                                ?>
                                <div class="item">
                                    <a href="<?php echo $imagen['url']; ?>" data-rel="portafolio" rel="<?php the_slug(); ?>">
                                        <?php echo wp_get_attachment_image($imagen['ID'], $size) ?>
                                    </a>
                                </div>
                                <?php

                            endwhile;
                        ?>
                        </div>
                    <?php else: ?>

                        <?php the_post_thumbnail(array(600, 1000),array('class'=>' img-responsive')); ?>

                    <?php endif; ?>

                </div>
                <div class="col-sm-6">
                    <?php if( have_rows('servicios') ): ?>

                    <div class="services area-information">
                        <div>
                            <h3>Servicio:</h3>
                        </div>
                        <div>
                            <?php
                                while ( have_rows('servicios') ) : the_row();
                                    the_sub_field('servicio');
                                    echo "<span>, </span>";
                                endwhile;
                            ?>
                        </div>    
                    </div>

                    <?php endif; ?>
                    
                    <div class="description area-information">
                        <div>
                            <h3>Descripcion: </h3>
                        </div>
                        <div class="text-justify">
                            <?php the_content(); ?>
                        </div>    
                    </div>
                    
                    <div class="links area-information">
                        <div>
                            <?php if(get_field('url')): ?>
                            <h3><i class="fa fa-link"></i> Enlace</h3>
                            <a href="<?php the_field('url'); ?>" target="_blank"><?php the_field('url'); ?></a>
                            <?php endif; ?>
                        </div>
                    </div>

                    <div class="tags area-information">
                        <div>
                            <?php if(has_tag()): ?>
                            <h3><i class="fa fa-tag"></i> Etiquetas</h3>
                            <?php the_tags('',', ',''); ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
    <!-- fin main -->

    <!-- buscador -->
    <?php get_template_part("templates-parts/buscador"); ?>
    <!-- fin buscador -->
    
    <!-- Footer -->
    <?php get_footer(); ?>
    <!-- Fin Footer -->