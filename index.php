<?php get_header(); ?>
    <!-- menu -->
    <?php get_template_part("templates-parts/menu"); ?>
    <!-- fin menu -->

    <!-- header -->
    <header>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1><?php the_title(); ?></h1>
                </div>
            </div>
        </div>
    </header>
    <!-- fin header -->

    <?php if(get_field('portafolio_home')): ?>
    <!-- portafolio -->
    <section id="portafolio">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
              <h2>Nuestro Trabajo</h2>
              <hr>
          </div>
        </div>
        <div class="row">
          <?php get_template_part("templates-parts/portfolio-home"); ?>
        </div>
      </div>
    </section>
    <!-- fin portafolio -->
    <?php else: ?>
    <!-- main -->
    <section id="main">
      <div class="container">

        <?php if( get_field('subtitulo') ): ?>
          <div class="row">
              <div class="col-lg-12 text-center">
                <h2><?php the_field('subtitulo'); ?></h2>
                <hr>
              </div>
          </div>
        <?php endif; ?>

        <div class="row">
            <div class="col-md-10 col-md-offset-1 text-justify">
                <?php the_content(); ?>
            </div>
        </div>

      </div>
    </section>
    <!-- fin main -->
  <?php endif; ?>
  
    <?php if( get_field('seccion_2') ): ?>
    <!-- seccion 2 -->
      <section>
        <div class="container">
          
          <?php if( get_field('subtitulo_seccion_2') ): ?>
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2><?php the_field('subtitulo_seccion_2'); ?></h2>
                    <hr>
                </div>
            </div>
          <?php endif; ?>

          <div class="row">
            <div class="col-md-12 text-justify">
                <?php the_field('seccion_2'); ?>
              </div>
          </div>
        </div>
      </section>
    <!-- fin seccion 2 -->
    <?php endif; ?>

    <?php if( get_field('seccion_3') ): ?>
    <!-- seccion 3 -->
      <section>
        <div class="container">
          
          <?php if( get_field('subtitulo_seccion_3') ): ?>
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2><?php the_field('subtitulo_seccion_3'); ?></h2>
                    <hr>
                </div>
            </div>
          <?php endif; ?>

          <div class="row">
            <div class="col-md-10 col-md-offset-1 text-justify">
                <?php the_field('seccion_3'); ?>
              </div>
          </div>
        </div>
      </section>
    <!-- fin seccion 3 -->
    <?php endif; ?>

    <?php if( get_field('seccion_4') ): ?>
    <!-- seccion 4 -->
      <section>
        <div class="container">

          <?php if( get_field('subtitulo_seccion_4') ): ?>
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2><?php the_field('subtitulo_seccion_4'); ?></h2>
                    <hr>
                </div>
            </div>
          <?php endif; ?>

          <div class="row">
            <div class="col-md-10 col-md-offset-1 text-justify">
                <?php the_field('seccion_4'); ?>
              </div>
          </div>
        </div>
      </section>
    <!-- fin seccion 4 -->
    <?php endif; ?>

    <?php if( get_field('seccion_5') ): ?>
    <!-- seccion 5 -->
      <section>
        <div class="container">

          <?php if( get_field('subtitulo_seccion_5') ): ?>
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2><?php the_field('subtitulo_seccion_5'); ?></h2>
                    <hr>
                </div>
            </div>
          <?php endif; ?>

          <div class="row">
            <div class="col-md-10 col-md-offset-1 text-justify">
                <?php the_field('seccion_5'); ?>
              </div>
          </div>
        </div>
      </section>
    <!-- fin seccion 5 -->
    <?php endif; ?>

    <!-- buscador -->
    <?php get_template_part( 'templates-parts/buscador' ); ?>
    <!-- fin buscador -->
    
  <!-- Footer -->
  <?php get_footer(); ?>
  <!-- Fin Footer -->