<?php get_header(); ?>
    <!-- menu -->
    <?php get_template_part("templates-parts/menu"); ?>
    <!-- fin menu -->
  
    <!-- header -->
    <header>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>Página no encontrada</h1>
                </div>
            </div>
        </div>
    </header>
    <!-- fin header -->

    <!-- main -->
    <section id="main">
      <div class="container">

		<div class="row">
            <div class="col-md-8 col-md-offset-2 text-center">
                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/img-404.png" alt="Página no encontrada - Por favor verifique la dirección url" class="img-responsive center-block" alt="Error 404">
            </div>
        </div>

      </div>
    </section>
    <!-- Fin main -->

    <!-- buscador -->
    <?php get_template_part("templates-parts/buscador"); ?>
    <!-- fin buscador -->
    
    <!-- Footer -->
    <?php get_footer(); ?>
    <!-- Fin Footer -->