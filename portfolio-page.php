<?php
/*
Template Name: Plantilla Portafolio
*/
?>
<?php get_header(); ?>
    <!-- menu -->
    <?php get_template_part("templates-parts/menu"); ?>
    <!-- fin menu -->

    <!-- header -->
    <header>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1><?php the_title(); ?></h1>
                </div>
            </div>
        </div>
    </header>
    <!-- fin header -->

    <!-- main -->
    <section id="main">
      <div class="container">

        <?php if( get_field('subtitulo') ): ?>
          <div class="row">
              <div class="col-lg-12 text-center">
                <h2><?php the_field('subtitulo'); ?></h2>
                <hr class="star-primary">
              </div>
          </div>
        <?php endif; ?>

        <div class="row">
            <div class="col-md-10 col-md-offset-1 text-justify">
                <?php the_content(); ?>
            </div>
        </div>

        <!-- portafolio -->
          <div class="row">
            <?php get_template_part("templates-parts/portfolio"); ?>
          </div>
        <!-- fin portafolio -->

      </div>
    </section>
    <!-- fin main -->
  
    <?php if( get_field('seccion_2') ): ?>
    <!-- seccion 2 -->
      <section>
        <div class="container">
          <div class="row">
            <div class="col-md-10 col-md-offset-1 text-justify">
                <?php the_field('seccion_2'); ?>
              </div>
          </div>
        </div>
      </section>
    <!-- fin seccion 2 -->
    <?php endif; ?>

    <?php if( get_field('seccion_3') ): ?>
    <!-- seccion 3 -->
      <section>
        <div class="container">
          <div class="row">
            <div class="col-md-10 col-md-offset-1 text-justify">
                <?php the_field('seccion_3'); ?>
              </div>
          </div>
        </div>
      </section>
    <!-- fin seccion 3 -->
    <?php endif; ?>

    <?php if( get_field('seccion_4') ): ?>
    <!-- seccion 4 -->
      <section>
        <div class="container">
          <div class="row">
            <div class="col-md-10 col-md-offset-1 text-justify">
                <?php the_field('seccion_4'); ?>
              </div>
          </div>
        </div>
      </section>
    <!-- fin seccion 4 -->
    <?php endif; ?>

    <?php if( get_field('seccion_5') ): ?>
    <!-- seccion 5 -->
      <section>
        <div class="container">
          <div class="row">
            <div class="col-md-10 col-md-offset-1 text-justify">
                <?php the_field('seccion_5'); ?>
              </div>
          </div>
        </div>
      </section>
    <!-- fin seccion 5 -->
    <?php endif; ?>

    <!-- buscador -->
    <?php get_template_part("templates-parts/buscador"); ?>
    <!-- fin buscador -->
    
  <!-- Footer -->
  <?php get_footer(); ?>
  <!-- Fin Footer -->