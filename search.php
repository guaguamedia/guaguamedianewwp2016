<?php get_header(); ?>
    <!-- menu -->
    <?php get_template_part("templates-parts/menu"); ?>
    <!-- fin menu -->

    <!-- header -->
    <header>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1>Buscador</h1>
                </div>
            </div>
        </div>
    </header>
    <!-- fin header -->

    <!-- main -->
    <section id="main">
      <div class="container">
        
        <div class="row">
            <div class="col-lg-12">
                <h3>Resultados para: <?php echo get_query_var('s') ?></h3>
            </div>
        </div>

        <!-- portafolio -->
        <?php 
        $s=get_search_query();
        $argsBusquedaPortafolio = array(
            's' =>$s,
            'post_type' => 'portafolio'
        );

        // The Query
        $queryBusquedaPortafolio = new WP_Query( $argsBusquedaPortafolio );
        if ( $queryBusquedaPortafolio->have_posts() ) :
        ?>
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2>Portafolio</h2>
                <hr class="star-light">
            </div>
        </div>
        <div class="row">
            <div class="col-md-10 col-md-offset-1 text-justify">
                
                <?php
                    while ( $queryBusquedaPortafolio->have_posts() ) {
                       $queryBusquedaPortafolio->the_post();
                         ?>
                            <div class="row search-list">
                                <div class="col-sm-12">
                                    <div class="col-sm-2">
                                        <?php the_post_thumbnail('thumbnail', array( 'class'=>' img-responsive')); ?>
                                    </div>
                                    <div class="col-sm-10">
                                        <h5>
                                            <a href='<?php the_permalink(); ?>'><?php the_title(); ?></a>
                                        </h5>
                                        <?php the_excerpt(); ?>
                                    </div>
                                </div>
                            </div>
                         <?php
                    }
                ?>

            </div>
        </div>
        <?php
        endif;
        wp_reset_query();
        ?>

        <!-- blog -->
        <?php 
        $s=get_search_query();
        $argsBusquedaPost = array(
            's' =>$s,
            'post_type' => 'post'
        );

        // The Query
        $queryBusquedaPost = new WP_Query( $argsBusquedaPost );
        if ( $queryBusquedaPost->have_posts() ) :
        ?>
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2>Blog</h2>
                <hr class="star-light">
            </div>
        </div>
        <div class="row">
            <div class="col-md-10 col-md-offset-1 text-justify">
                
                <?php
                    while ( $queryBusquedaPost->have_posts() ) {
                       $queryBusquedaPost->the_post();
                         ?>
                            <div class="row search-list">
                                <div class="col-sm-12">
                                    <div class="col-sm-2">
                                        <?php the_post_thumbnail('thumbnail', array( 'class'=>' img-responsive')); ?>
                                    </div>
                                    <div class="col-sm-10">
                                        <h5>
                                            <a href='<?php the_permalink(); ?>'><?php the_title(); ?></a>
                                        </h5>
                                        <?php the_excerpt(); ?>
                                    </div>
                                </div>
                            </div>
                         <?php
                    }
                ?>

            </div>
        </div>
        <?php
        endif;
        wp_reset_query();
        ?>

      </div>
    </section>
    <!-- fin main -->

    <!-- buscador -->
    <?php get_template_part("templates-parts/buscador"); ?>
    <!-- fin buscador -->
    
    <!-- Footer -->
    <?php get_footer(); ?>
    <!-- Fin Footer -->