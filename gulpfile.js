'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var minifyCSS = require('gulp-minify-css');
var watch = require('gulp-watch');
var minify = require('gulp-minify');
var rename = require('gulp-rename');

gulp.task('sass', function () {
   gulp.src('./sass/main.scss')
   .pipe(sass())
   .pipe(minifyCSS())
   .pipe(rename({
	    suffix: '-min'
	}))
   .pipe(gulp.dest('./css'));
});

gulp.task('js', function() {
  	gulp.src('./js/script.js')
    .pipe(minify())
    .pipe(gulp.dest('./js'));
});

gulp.task('default', ['sass', 'js'], function () {
   gulp.watch(['./sass/**/*.scss'], ['sass']);
   gulp.watch(['./js/**/*.js'], ['js']);
});
