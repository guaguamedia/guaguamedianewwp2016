    <footer>
        <div class="footer-above">
            <div class="container">
                <div class="row">
                    <div class="footer-col col-md-4 col-sm-6">
                        <?php dynamic_sidebar('left'); ?>
                    </div>
                    <div class="footer-col col-md-4 col-sm-6">
                        <?php dynamic_sidebar('center'); ?>
                    </div>
                    <div class="footer-col col-md-4 col-sm-12">
                        <?php dynamic_sidebar('right'); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-below">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <?php dynamic_sidebar('copy'); ?>
                    </div>
                </div>
            </div>
        </div>
    </footer>

</div>

<?php wp_footer(); ?>

</body>

</html>