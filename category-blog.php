<?php get_header(); ?>
    <!-- menu -->
    <?php get_template_part("templates-parts/menu"); ?>
    <!-- fin menu -->

    <!-- header -->
    <header>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1><?php echo single_cat_title("", false); ?></h1>
                </div>
            </div>
        </div>
    </header>
    <!-- fin header -->

    <!-- main -->
    <section id="main">
      <div class="container">

        <?php
            $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
            $cat = (get_query_var('cat')) ? get_query_var('cat') : 1;
            $args = array(
                'cat' => $cat,
                'paged' => $paged
            );

            query_posts($args);

            if (have_posts()): while (have_posts()) : the_post();
        ?>

        <div class="row item-blog">
            <div class="col-lg-8 col-lg-offset-2">
                <h2><?php the_title(); ?></h2>
            </div>
            <div class="col-lg-8 col-lg-offset-2">
              <a href="<?php the_permalink(); ?>">
                <?php the_post_thumbnail(array(700, 600),array('class'=>' img-responsive')); ?>
              </a>
            </div>
            <div class="col-lg-8 col-lg-offset-2">
                <p class="text-justify">
                    <?php the_excerpt(); ?>
                </p>
            </div>
            <div class="col-lg-8 col-lg-offset-2">
                <ul class="list-inline">
                    <li>
                        <i class="fa fa-fw fa-calendar"></i> <?php the_time('l j \d\e F \d\e\l Y') ?>
                    </li>
                    <li>
                        <i class="fa fa-fw fa-user"></i> <?php the_author_posts_link(); ?>
                    </li>
                    <?php if(has_tag()): ?>
                    <li>
                        <i class="fa fa-fw fa-tag"></i> <?php the_tags('',', ',''); ?>
                    </li>
                    <?php endif; ?>
                </ul>
            </div>
        </div>

        <?php
          endwhile;
              ?>
              <div class="row">
                <div class="col-lg-8 col-lg-offset-2 text-center">
                    <?php wp_pagenavi(); ?>
                </div>
            </div>
          <?php
          else: ?>
              <p><?php _e('Lo sentimos, no existen entradas.'); ?></p>
          <?php
          endif;
        ?>

      </div>
    </section>
    <!-- fin main -->

    <!-- buscador -->
    <?php get_template_part("templates-parts/buscador"); ?>
    <!-- fin buscador -->
    
  <!-- Footer -->
  <?php get_footer(); ?>
  <!-- Fin Footer -->