<?php

if ( ! isset( $content_width ) ) $content_width = 900;

if ( ! function_exists( 'guaguamedia_setup' ) ) :

	function guaguamedia_setup() {

		add_theme_support( 'automatic-feed-links' );

		add_theme_support( 'title-tag' );

		add_theme_support( 'post-thumbnails' );
		// set_post_thumbnail_size( 825, 510, true );

		register_nav_menus( array(
			'primary_menu' => __( 'Menu Principal',	'guaguamedia' ),
			'footer_menu'  => __( 'Menu Footer',	'guaguamedia' )
		) );

		add_theme_support( 'html5', array(
			'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
		) );

		add_theme_support( 'post-formats', array(
			'aside', 'image', 'video', 'quote', 'link', 'gallery', 'status', 'audio', 'chat'
		) );

		//add_editor_style( array( 'css/editor-style.css', 'genericons/genericons.css') );

	}

endif;
add_action( 'after_setup_theme', 'guaguamedia_setup' );

require get_template_directory() . '/inc/page-navi.php';

require get_template_directory() . '/inc/widgets.php';

require get_template_directory() . '/inc/wp_bootstrap_navwalker.php';

require get_template_directory() . '/inc/extra.php';

require get_template_directory() . '/inc/enqueue.php';

?>
