<?php get_header(); ?>

    <!-- menu -->
    <?php get_template_part("templates-parts/menu"); ?>
    <!-- fin menu -->
  
    <!-- header -->
    <header>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1><?php the_title(); ?></h1>
                </div>
            </div>
        </div>
    </header>
    <!-- fin header -->

    <!-- main -->
    <section id="main">
        <div class="container">
            
            <div class="row content-blog">
                <div class="col-lg-8 col-lg-offset-2">
                    <?php the_post_thumbnail(array(700, 600),array('class'=>' img-responsive')); ?>
                </div>
            </div>
            <div class="row content-blog">
                <div class="col-lg-8 col-lg-offset-2 text-justify">
                    <?php the_content(); ?>
                </div>
            </div>
            <div class="row content-blog">
                <div class="col-lg-8 col-lg-offset-2">
                    <ul class="list-inline">
                        <li>
                            <i class="fa fa-fw fa-calendar"></i> <?php the_time('l j \d\e F \d\e\l Y') ?>
                        </li>
                        <li>
                            <i class="fa fa-fw fa-user"></i> <?php the_author_posts_link(); ?>
                        </li>
                        <?php if(has_tag()): ?>
                            <li>
                                <i class="fa fa-fw fa-tag"></i> <?php the_tags('',', ',''); ?>
                            </li>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
            <div class="row content-blog">
                <div class="col-lg-8 col-lg-offset-2">
                    <div class="addthis_sharing_toolbox"></div>
                </div>
            </div>

            <?php
                $orig_post = $post;
                global $post;
                $tags = wp_get_post_tags($post->ID);
                 
                if ($tags) {
                $tag_ids = array();
                foreach($tags as $individual_tag) $tag_ids[] = $individual_tag->term_id;
                $argsPostRelacionados=array(
                'tag__in' => $tag_ids,
                'post__not_in' => array($post->ID),
                'posts_per_page'=>3, // Number of related posts to display.
                'caller_get_posts'=>1
                );
                 
                $queryPostRelacionados = new WP_Query( $argsPostRelacionados );
                if($queryPostRelacionados->have_posts()):
            ?>           

            <div class="row">
                <div class="col-lg-10 col-lg-offset-1">
                    <h3>Entradas Relacionadas</h3>
                </div>
                <div class="col-lg-10 col-lg-offset-1">
                    <?php
                 
                    while( $queryPostRelacionados->have_posts() ) {
                    $queryPostRelacionados->the_post();
                    ?>

                    <div class="col-md-4">
                        <a href="<?php the_permalink()?>">
                            <?php the_post_thumbnail(array(390, 700),array('class'=>' img-responsive')); ?>
                        </a>
                        <h4><?php the_title(); ?></h4>
                        <i class="fa fa-fw fa-calendar"></i> <?php the_time('l j \d\e F \d\e\l Y') ?>
                    </div>
                     
                    <?php } ?>
                </div>
            </div>

            <?php endif;
                    }
                    $post = $orig_post;
                    wp_reset_query();
            ?>

        </div>
    </section>
    <!-- fin main -->

    <!-- buscador -->
    <?php get_template_part("templates-parts/buscador"); ?>
    <!-- fin buscador -->
    
    <!-- Footer -->
    <?php get_footer(); ?>
    <!-- Fin Footer -->